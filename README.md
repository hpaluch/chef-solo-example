# Chef solo example

> DISCLAIMER
>
> Chef has confusing licensing. Ensure that you have valid license before using Chef products.
> 
> Chef Licensing related articles:
> 
> - https://searchitoperations.techtarget.com/news/252460992/Chef-licensing-changes-prompt-debate-among-IT-pros
> - https://www.thelonelyghost.com/posts/chef-goes-redhat/
>
> My understanding is:
> - source code is free under ASF (BSDlike license)
> - but binaries are distribued under proprietary license - EULA for non-commercial, paid MSLA for commercials
>
> So it is like RHEL Linux - source is freely available but binaries are restricted.
>
> So if you are commercial and want to use Chef for free you have only option (like making CentOS from RHEL sources):
> - download Chef sources
> - rename them 
> - build them yourself and install them yourself.


Here is introductory example that uses both:
- [chef-solo](https://github.com/chef/chef/tree/master/chef-bin) to perform tasks locally without Chef-Server
- [berkshelf](https://github.com/berkshelf/berkshelf) to manage external cookbook `apache2`
  that is used by this example recipe

It may look obvious, but I would never do it (using `chef-solo` + `berkshelf` together) without this guide:
- https://blog.differentpla.net/blog/2014/11/30/starting-with-chef-solo/


# Setup

There are two alternatives:
- easy single-package Chef-Workstation, but you must accept EULA (for Personal,
  Development and Test use only) right before download
- more difficult installation from Ruby GEMS  - theoretically they should be under ASF.
  But Chef will force you to Accept that EULA on first run...

So from licensing standpoint you will loose anyway...


## Setup using Chef-Workstation package

Note: We will use whole Chef-Workstation to have all commands available including `berks`.

Tested on Debian10/amd64:

```bash
sudo apt-get install git curl
# install chef-workstation
curl -O https://packages.chef.io/files/stable/chef-workstation/20.10.168/debian/10/chef-workstation_20.10.168-1_amd64.deb
sudo dpkg -i chef-workstation_20.10.168-1_amd64.deb
```

Now append these lines to your `~/.bashrc`:

```bash
PATH=/opt/chef-workstation/bin:$PATH
export PATH
```

And source it to have `chef-*` binaries in your PATH:

```bash
source ~/.bashrc
```

## Setup using GEMs installation

We will depend on stock Debian10 Ruby. However we need to pin gems version to these
that supports Ruby 2.5.

Install these packages and/or gems:

```shell
sudo apt-get install -y ruby ruby-dev make gcc g++ git
# Currently Debian10 has Ruby 2.5, so we must pin Gems which support such version
sudo gem install ohai -v 16.5.6
sudo gem install chef-zero -v 14.0.17
sudo gem install chef -v 15.14.0
# includes chef-solo command:
sudo gem install chef-bin -v 15.14.0
sudo gem install berkshelf
```

## Setup (continued)

Now checkout this project to suitable directory:

```bash
mkdir -p ~/projects/
cd ~/projects
git clone https://gitlab.com/hpaluch/chef-solo-example.git 
cd chef-solo-example/
```

And continue to next section...

# Running

Just invoke `./run_apache2_ex_recipe.sh` and it should create simple resource
from external [apache2](https://supermarket.chef.io/cookbooks/apache2) cookbook to install Apache2 web server.

```bash
./run_apache2_ex_recipe.sh
```

> DISCLAIMER
>
> You still will be likely asked to Accept Chef EULA, but it does NOT cover commercial use. 
> Therefore be careful!
> 
> ```
>             Chef License Acceptance
> 
> Before you can continue, 2 product licenses
> must be accepted. View the license at
> https://www.chef.io/end-user-license-agreement/
> 
> Licenses that need accepting:
>   * Chef Infra Client
>   * Chef InSpec
> 
> Do you accept the 2 product licenses (yes/no)?
> ```
 

# Resources

This project is based on work on many other users including:
- https://blog.differentpla.net/blog/2014/11/30/starting-with-chef-solo/
- https://medium.com/@chandra25ms/running-a-chef-cookbook-using-chef-solo-bb4b2773acb8
- https://www.oreilly.com/library/view/learning-chef/9781491945087/ch04.html
- https://stackoverflow.com/a/21138545


