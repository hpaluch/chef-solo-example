#!/bin/bash

set -e
set -o pipefail
cd `dirname $0`
set -x
berks install
berks vendor cookbooks/
set +x
which chef-solo || {
	echo "chef-solo not found in PATH" >&2
	exit 1
}
# sudo often has restricted PATH, so we must specify fullpath with which
# this will exit if chef-solo is not found...
set -x
sudo $(which chef-solo) -c `pwd`/solo_berks.rb -o 'recipe[example::apache2_ex]'
exit 0

